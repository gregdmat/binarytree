<!doctype html>
<html lang="pt-BR">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Árvore Binária</title>
</head>
<body>
    <div class="row">
        <div class="col-sm-12 bg-dark">
            <h1 class="text-center"><a href="{{route('home')}}" class="text-white">Árvore Binária</a></h1>
        </div>
    </div>

    <div class="row bg-light">


        <div class="col-sm-4">
            <button type="button" class="btn btn-success btn-lg btn-block" data-toggle="modal" data-target="#modal-form">ADICIONAR NÓ</button>
        </div>

        <div class="col-sm-4">
            <a class="btn btn-primary btn-lg btn-block" href="#" role="button">Nível da Árvore: {{$binaryTree['tree-level']}}</a>
        </div>

        <div class="col-sm-4">
            <a class="btn btn-primary btn-lg btn-block" href="#" role="button">Nós: {{$binaryTree['tree-nodes']}}</a>
        </div>
    </div>

    <div id="tree-view" class="container-fluid" style="margin-top:20px;">

        @foreach($binaryTree['nodes'] as $key => $node)
            <div class="row">
                <div class="col-sm-1">
                    <h4>Nível: </h4>
                </div>
                <div class="col-sm-1">
                    <h4>{{$node['level']}}</h4>
                </div>
                <div class="col-sm-9">
                    <div class="progress">
                        <div class="progress-bar bg-success" role="progressbar" style="width:{{100 / $node['level']}}%" aria-valuemax="100"></div>
                    </div>
                    {{$node['name'] . ", " . $node['phone'] . ", " . $node['birthday']}}
                </div>
                <div class="col-sm-1">
                    <a type="button" class="btn btn-danger btn-sm" href="{{route('delete', ['key' => $key, 'name' => $node['name']])}}">EXCLUIR</a>
                </div>
            </div>
        @endforeach
    </div>

    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Adicionar Nó</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="form" action="{{route('create')}}" method="post">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label for="name">Nome</label>
                            <input required type="text" class="form-control" id="name" name="name">
                        </div>
                        <div class="form-group">
                            <label for="Telefone">Telefone</label>
                            <input required type="number" class="form-control" id="phone" name="phone">
                        </div>
                        <div class="form-group">
                            <label for="date">Nascimento</label>
                            <input required type="date" class="form-control" name="birthday">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
                    <button type="submit" form="form" class="btn btn-success">Salvar</button>
                </div>
            </div>
        </div>
    </div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script>
    // this is the id of the form
/*    $("#form").submit(function(e) {


        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                alert("Ok");
            },
            error:function(){
                alert("An error has occured !");
            }
        });

        e.preventDefault(); // avoid to execute the actual submit of the form.
    }); */
</script>
</body>
</html>

