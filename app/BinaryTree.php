<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BinaryTree extends Model
{
    public $treeRoot;
    public $nodes = 0;
    public $level = 0;
    public $postOrderArray = [
        'nodes' => array()
    ];

    // incrementa árvore
    public function add(Node $node)
    {
        if(is_null($this->treeRoot))
        {
            $node->level = 1;
            $this->treeRoot = $node;
        }else
        {
            $root = $this->scroll($this->treeRoot, $node);
            if($root->thisHigher($node->entity->name, 0)) {
                $node->root = $root;
                $root->left = $node;
            }
            else
            {
                $node->root = $root;
                $root->right = $node;
            }
        }

        $this->nodes++;
        if($node->level > $this->level)
        {
            $this->level = $node->level;
        }
    }

    //função recursiva para percorrer árvore
    public function scroll(Node $currentNode,Node $node) : Node
    {
        $node->level = $currentNode->level +1;

        if($currentNode->thisHigher($node->entity->name, 0))
        {
            if(is_null($currentNode->left))
            {
                return $currentNode;
            }

            return $this->scroll($currentNode->left, $node);
        }else
        {
            if(is_null($currentNode->right))
            {
                return $currentNode;
            }

           return $this->scroll($currentNode->right, $node);
        }
    }

    public function resetPostOrderArray()
    {
        $this->postOrderArray['nodes'] = [];
        $this->postOrderArray['tree-level'] = $this->level;
        $this->postOrderArray['tree-nodes'] = $this->nodes;
        $this->definePostOrderArray($this->treeRoot);
    }

    public function definePostOrderArray($currentNode)
    {
        if(!is_null($currentNode))
        {
            $this->definePostOrderArray($currentNode->left);
            $this->definePostOrderArray($currentNode->right);
            array_push(
                $this->postOrderArray['nodes'],
                [
                    'level'     => $currentNode->level,
                    'name'      => $currentNode->entity->name,
                    'phone'     => $currentNode->entity->phone,
                    'birthday'  => $currentNode->entity->birthday
                ]
            );
            $currentNode->key = max(array_keys($this->postOrderArray['nodes']));
        }
    }

    public function search($key, $name, $node = null)
    {
        if(is_null($node))
            $node = $this->treeRoot;

        if($node->key == $key)
            return $node;

        if($node->thisHigher($name))
            return $this->search($key, $name, $node->left);
        else
            return $this->search($key, $name, $node->right);
    }

    public function exclude(Node $toExcludeNode)
    {
        //SEM FILHOS
        if(is_null($toExcludeNode->left) AND is_null($toExcludeNode->right))
        {
            if($toExcludeNode->level>1)
            {
                if($this->rootLinkIsLeft($toExcludeNode))

                    $toExcludeNode->root->left = null;
                else
                    $toExcludeNode->root->right = null;
            }else
                $this->treeRoot = null;
        }

        //APENAS FILHO ESQUERDA
        if(((!is_null($toExcludeNode->left)) AND is_null($toExcludeNode->right)))
        {
            if($this->rootLinkIsLeft($toExcludeNode))
            {
                $toExcludeNode->left->root = $toExcludeNode->root;
                $toExcludeNode->root->left = $toExcludeNode->left;
            }else
            {
                $toExcludeNode->left->root = $toExcludeNode->root;
                $toExcludeNode->root->right = $toExcludeNode->left;
            }
        }

        //APENAS FILHO DIREITA
        if(((is_null($toExcludeNode->left)) AND is_null(!$toExcludeNode->right)))
        {
            if($this->rootLinkIsLeft($toExcludeNode))
            {
                $toExcludeNode->right->root = $toExcludeNode->root;
                $toExcludeNode->root->left = $toExcludeNode->right;
            }else
            {
                $toExcludeNode->left->root = $toExcludeNode->root;
                $toExcludeNode->root->right = $toExcludeNode->right;
            }
        }

        //DOIS FILHOS
        if((!is_null($toExcludeNode->left)) AND is_null(!$toExcludeNode->right))
        {
            $leftHigher = $this->getHigherLeftNode($toExcludeNode->left);
            if($this->rootLinkIsLeft($toExcludeNode))
            {
                $leftHigher->left = $leftHigher->root->right;
                $leftHigher->root = $toExcludeNode->root;
                $toExcludeNode->root->left = $leftHigher;
            }else
            {
                $leftHigher->left = $leftHigher->root->right;
                $leftHigher->root = $toExcludeNode->root;
                $toExcludeNode->root->right = $leftHigher;
            }
        }

        $this->nodes --;
        $this->level = "?";
    }

    public function getHigherLeftNode(Node $node, Node $higher = null)
    {
        if(is_null($higher) OR (!$higher->thisHigher($node)))
            $higher = $node;

        if(!is_null($node->right))
        {
            $this->getHigherLeftNode($node->right);
        }

        return $higher;
    }

    //RETORNA true SE O TO EXCLUDE FOR A ESQUERDA DA RAIZ OU false SE FOR DIREITA
    public function rootLinkIsLeft(Node $toExclude)
    {
        if(!isset($toExclude->root->left))
            return 0;

        if($toExclude->root->left->key == $toExclude->key)
            return 1;
        else
            return 0;
    }
}
