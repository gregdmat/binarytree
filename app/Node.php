<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Node extends Model
{
    public $key;
    public $root;
    public $left;
    public $right;
    public $entity;
    public $level;

    public function __construct(Entity $entity)
    {
        $this->entity = $entity;
    }

    //DETERMINA SE O NÓ ATUAL É ESQUERDA(<) OU DIREITA(>=) QUE O NOVO
    public function thisHigher($name, $i = 0)
    {
        if($this->entity->name == $name)
            return false;

        if(strlen($this->entity->name)< $i+1)
            return false;

        if(strlen($name) < $i+1)
            return true;

        if((ord($this->entity->name[$i]) > ord($name[$i])))
            return true;

        elseif((ord($this->entity->name[$i]) < ord($name[$i])))
            return false;

        else
        {
            $i++;
            $this->thisHigher($name, $i);
        }
    }
}
