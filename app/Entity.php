<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entity extends Model
{
    public $name;
    public $phone;
    public $birthday;

    public function __construct($arrayData)
    {
        $this->name = $this->stringNormalize($arrayData['name']);
        $this->phone = $arrayData['phone'];
        $this->birthday = $arrayData['birthday'];
    }


    public function stringNormalize(String $string)
    {
        return strtoupper (
            preg_replace(
                array(
                    "/(á|à|ã|â|ä)/",
                    "/(Á|À|Ã|Â|Ä)/",
                    "/(é|è|ê|ë)/",
                    "/(É|È|Ê|Ë)/",
                    "/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/",
                    "/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/",
                    "/(ú|ù|û|ü)/",
                    "/(Ú|Ù|Û|Ü)/",
                    "/(ñ)/","/(Ñ)/"
                ),
                explode(
                    " ",
                    "a A e E i I o O u U n N"
                ),
                $string
            )
        );
    }
}
