<?php

namespace App\Http\Controllers;

use App\BinaryTree;
use App\Entity;
use App\Node;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public $binaryTree;

    public function __construct(BinaryTree $binaryTree)
    {
        $this->binaryTree = $binaryTree;
    }

    //CRIA ARVORE OU RECUPERA ARVORE EXISTENTE
    public function index()
    {
        if(!is_null(session('tree')))
            $this->binaryTree = session('tree')[0];
        $this->binaryTree->resetPostOrderArray();

        return view('basic', $this->binaryTree)->with('binaryTree', $this->binaryTree->postOrderArray);
    }

    //CRIA NOVO NÓ
    public function create(Request $request)
    {
        $entity = new Entity($request->all());
        $node = new Node($entity);

        if(!$request->session()->exists('tree'))
        {
            $this->binaryTree->add($node);
        }else
        {
            $this->binaryTree = $request->session()->pull('tree')[0];
            $this->binaryTree->add($node);

        }

        $this->binaryTree->resetPostOrderArray();
        $request->session()->push('tree',$this->binaryTree);
        return view('basic')->with('binaryTree', $this->binaryTree->postOrderArray);
    }

    //EXCLUI UM NO
    public function delete($key, $name)
    {
        if(!is_null(session('tree')))
            $this->binaryTree = session('tree')[0];

        $toExcludeNode = $this->binaryTree->search($key, $name);
        $this->binaryTree->exclude($toExcludeNode);

        $this->binaryTree->resetPostOrderArray();
        return view('basic')->with('binaryTree', $this->binaryTree->postOrderArray);
    }

}
